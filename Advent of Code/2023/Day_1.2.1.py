file = "Input/Day_1.txt"
lines = {}
numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six',
           'seven', 'eight', 'nine', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ]
letter_numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']

number_dict = {}
r_number_dict = {}

min_number_list = []
max_number_list = []
values = []

calibration_sum = 0

with open(file) as file_object:
    counter = 1
    for line in file_object:
        lines[counter] = line
        counter += 1

for line_number, line in lines.items():
    index_number_dict = {}
    reversed_index_number_dict = {}
    for number in numbers:
        f_number = line.find(number)
        if f_number != -1:
            if number in letter_numbers:
                index_number_dict[f_number] = letter_numbers.index(number)
            else:
                index_number_dict[f_number] = number

        rf_number = line.rfind(number)
        if rf_number != -1:
            if number in letter_numbers:
                reversed_index_number_dict[rf_number] = letter_numbers.index(number)
            else:
                reversed_index_number_dict[rf_number] = number

    min_number = []
    for index_number in index_number_dict:
        min_number.append(index_number)
    min_number_list.append(str(index_number_dict[min(min_number)]))

    max_number = []
    for rindex_number in reversed_index_number_dict:
        max_number.append(rindex_number)
    max_number_list.append(str(reversed_index_number_dict[max(max_number)]))

for i in range(len(min_number_list)):
    values.append(int(min_number_list[i] + max_number_list[i]))

for value in values:
    calibration_sum += value

print(calibration_sum)