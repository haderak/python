file = "Input/Day_1.txt"
lines = {}
numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six',
           'seven', 'eight', 'nine', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ]
letter_numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
calibration_values = {}
reversed_calibration_values = {}
min_values = []
max_values = []
values = []
calibration_sum = 0

with open(file) as file_object:
    counter = 1
    for line in file_object:
        lines[counter] = line
        counter += 1

for k, v in lines.items():
    index_dict = {}
    for number in numbers:
        fnumber = v.find(number)
        if fnumber != -1:
            if number in letter_numbers:
                index_dict[v.find(number)] = letter_numbers.index(number)
            else:
                index_dict[v.find(number)] = number
        else:
            continue
    calibration_values[k] = index_dict

for k, v in lines.items():
    index_dict = {}
    for number in numbers:
        rfnumber = v.rfind(number)
        if rfnumber != -1:
            if number in letter_numbers:
                index_dict[v.find(number)] = letter_numbers.index(number)
            else:
                index_dict[v.find(number)] = number
        else:
            continue
    reversed_calibration_values[k] = index_dict

for line, index in calibration_values.items():
    index_list = []
    for i in index:
        index_list.append(i)
    val = calibration_values[line][min(index_list)]
    min_values.append(str(val))

for line, index in reversed_calibration_values.items():
    index_list = []
    for i in index:
        index_list.append(i)
    val = reversed_calibration_values[line][max(index_list)]
    max_values.append((str(val)))

for i in range(len(min_values)):
    values.append(int(min_values[i] + max_values[i]))

print(values)

for i in values:
    calibration_sum += i

print(calibration_sum)

