file = "Input/Day_1.txt"
lines = {}
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
calibration_values = []
calibration_sum = 0

with open(file) as file_object:
    counter = 1
    for line in file_object:
        line_val = []
        for symbol in line:
            line_val.append(symbol)
        lines[counter] = line_val
        counter += 1

for k, v in lines.items():
    for value in v:
        if value in numbers:
            for rvalue in reversed(v):
                if rvalue in numbers:
                    calibration_values.append(value + rvalue)
                    break
            break
        else:
            continue

for i in calibration_values:
    calibration_sum += int(i)

print(calibration_sum)
