import re

pattern = re.compile("(\\d+)\\s(green|blue|red).*")


def main():
    total_value = 0
    file = open('Input/Day_2.txt', 'r')

    for line in file:
        power = 1
        colours = {"red": 0, "green": 0, "blue": 0}

        line = line[line.find(":") + 1:]
        sets = line.split(";")

        for set in sets:
            cubes = set.split(",")

            for cube_group in cubes:
                match = re.match(pattern, cube_group.strip())
                number = int(match.group(1))
                colour = match.group(2)

                if colours.get(colour) < number:
                    colours[colour] = number

        for k, v in colours.items():
            power *= v

        total_value += power
    file.close()

    print(total_value)


if __name__ == '__main__':
    main()
