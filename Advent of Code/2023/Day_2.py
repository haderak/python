import re

colours = {"red": 12, "green": 13, "blue": 14}
pattern = re.compile("(\\d+)\\s(green|blue|red).*")


def main():
    valid_games = 0
    file = open('Input/Day_2.txt', 'r')
    game = 0

    for line in file:
        game += 1

        line = line[line.find(":") + 1:]
        sets = line.split(";")

        try:
            for set in sets:
                cubes = set.split(",")

                for cube_group in cubes:
                    match = re.match(pattern, cube_group.strip())
                    number = int(match.group(1))
                    colour = match.group(2)

                    if colours.get(colour) < number:
                        raise Exception(print(f"Game {game} is invalid"))

            valid_games += game

        except Exception:
            pass

    file.close()

    print(valid_games)


if __name__ == '__main__':
    main()
