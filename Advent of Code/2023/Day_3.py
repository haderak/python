def main():
    lines = {}
    line_index = 0
    file = open("Input/Day_3.txt", 'r')

    numbers_and_coordinates = []

    for line in file:
        x = line.replace('\n', '')
        line = list(x)

        lines[line_index] = line
        line_index += 1

    for k in lines:
        v = lines[k]
        for x, v in enumerate(v):
            if v.isdigit():
                check_surroundings(lines, k, x, v, numbers_and_coordinates)

    sieved_out_numbers_and_coordinates = sieve_out_doubles(numbers_and_coordinates)
    grouped_numbers = group_numbers(sieved_out_numbers_and_coordinates, lines)
    print(grouped_numbers)

    sum = 0

    for number in grouped_numbers:
        sum += int(number)

    print(sum)


def check_surroundings(dict, i, j, numb, list):
    for y in range(i - 1, i + 2):
        try:
            if y < 0 or y >= len(dict):
                continue
            else:
                for x in range(j - 1, j + 2):
                    if x < 0 or x >= len(dict[y]):
                        continue
                    if dict[y][x] == "." or dict[y][x].isdigit():
                        continue
                    else:
                        list.append((numb, i, j))
                        raise

        except:
            break


def sieve_out_doubles(tuples_list):
    new_tuples_list = tuples_list.copy()
    counter = 0
    digit_y = new_tuples_list[0][1]
    digit_x = new_tuples_list[0][2]
    for index, tpl in enumerate(tuples_list):
        if tpl[1] == digit_y:
            if tpl[2] == digit_x + 1:
                new_tuples_list.pop(index - counter)
                counter += 1

        digit_y = tpl[1]
        digit_x = tpl[2]

    print(new_tuples_list)
    return new_tuples_list


def group_numbers(tuples_list, dictionary):
    digits_list = []
    for tpl in tuples_list:
        for k, v in dictionary.items():
            if k == tpl[1]:
                if v[tpl[2] - 1].isdigit():
                    if v[tpl[2] - 2].isdigit():
                        number = str(v[tpl[2] - 2]) + str(v[tpl[2] - 1]) + str(tpl[0])
                    elif v[tpl[2] + 1].isdigit():
                        number = str(v[tpl[2] - 1]) + str(tpl[0]) + str(v[tpl[2] + 1])
                    else:
                        number = str(v[tpl[2] - 1]) + str(tpl[0])
                elif v[tpl[2] + 1].isdigit():
                    number = str(tpl[0]) + str(v[tpl[2] + 1])
                    if v[tpl[2] + 2].isdigit():
                        number += str(v[tpl[2] + 2])
                else:
                    number = tpl[0]

                digits_list.append(number)

    return digits_list


if __name__ == '__main__':
    main()
