def main():
    lines = {48: 390, 98: 1103, 90: 1112, 83: 1360}  # Key = Time, Value = Distance
    # lines = {48989083: 390110311121360}
    larger_distances_numbers = []
    factor = 1

    for time, distance in lines.items():
        new_distances_list = []
        for speed in range(time + 1):
            new_distance = speed * (time - speed)
            if new_distance <= distance:
                continue
            else:
                new_distances_list.append(new_distance)

        larger_distances_numbers.append(len(new_distances_list))

    print(larger_distances_numbers)

    for larger_distance_number in larger_distances_numbers:
        factor *= larger_distance_number

    print(factor)


if __name__ == "__main__":
    main()
