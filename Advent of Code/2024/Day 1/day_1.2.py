file = "Input_day_1.txt"
list_1 = []
list_2 = []
num_list = []
sim_score = 0

with open(file) as file_object:
    for line in file_object:
        split_line = line.split()
        list_1.append(split_line[0])
        list_2.append(split_line[1])

for i in range(len(list_1)):
    list_1[i] = int(list_1[i])
    list_2[i] = int(list_2[i])

for i in list_1:
    a = list_2.count(i)
    sim_score += a * i

print(sim_score)


