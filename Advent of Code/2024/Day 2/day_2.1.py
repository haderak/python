filename = "Input_day_2.txt"

input_dict = {}
differences = {}

with open(filename) as file_data:
    i = 0
    for line in file_data:
        line_list = []
        numb_list = line.split()
        for n in range(len(numb_list)):
            numb_list[n] = int(numb_list[n])
        input_dict[i] = numb_list
        i += 1

for k in input_dict.keys():
    difference_list = []
    for i in range(len(input_dict[k]) - 1):
        a = input_dict[k][i + 1] - input_dict[k][i]
        difference_list.append(a)

    differences[k] = difference_list

right_differences = 0

for d in range(len(differences)):
    diff_list = differences[d]
    counter = 0
    for a in diff_list:
        if abs(a) in [1, 2, 3]:
            if a <= -1:
                counter += -1

            elif a >= 1:
                counter += 1
    if abs(counter) == len(diff_list):
        right_differences += 1

print(right_differences)
