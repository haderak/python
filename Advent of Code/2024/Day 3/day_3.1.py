import re

filename = "Input_day_3.txt"
clean_data = []
final_number = 0
data = ""

with open(filename) as datafile:
    for x in datafile:
        data += x

clean_data = re.findall("mul\(\d{1,3},\d{1,3}\)", data)

for mul in clean_data:
    numbers = re.findall("\d{1,3}", mul)
    final_number += int(numbers[0]) * int(numbers[1])

print(final_number)