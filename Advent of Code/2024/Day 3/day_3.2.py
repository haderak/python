import re

filename = "Input_day_3.txt"
clean_data = []
final_number = 0
data = ""

with open(filename) as datafile:
    for x in datafile:
        data += x

clean_data = re.findall("mul\(\d{1,3},\d{1,3}\)|don't\(\)|do\(\)", data)

clean_data_str = ""
for group in clean_data:
    clean_data_str += group

filtered_data = re.sub("don't\(\)(mul\(\d{1,3},\d{1,3}\))+|do\(\)", "", clean_data_str)

clean_filtered_data = re.findall("mul\(\d{1,3},\d{1,3}\)", filtered_data)
print(clean_filtered_data)

for mul in clean_filtered_data:
    numbers = re.findall("\d{1,3}", mul)
    final_number += int(numbers[0]) * int(numbers[1])

print(final_number)
