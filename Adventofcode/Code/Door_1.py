filename = 'text_files/AoC1.txt'
calory = []
calory_sum = []
total = 0

with open(filename) as file_object:
    for line in file_object:
        if line != '\n':
            calory.append(line)
        if line == '\n':
            for number in range(0, len(calory)):
                total = total + int(calory[number])
            calory_sum.append(total)
            total = 0
            calory = []

scalory_sum = sorted(calory_sum)

scalory_sum.reverse()
print(scalory_sum)

for number in range(0, 3):
    total = total + scalory_sum[number]

print(total)
