filename = 'text_files/AoC2.txt'

total = 0
# A = Rock = X ... 1
# B = Paper = Y ... 2
# C = Scissors = Z ... 3
# Lose ... 0
# Draw ... 3
# Win ... 6

with open(filename) as file_object:
    for line in file_object:
        if 'A X' in line:
            total += 4
        elif 'A Y' in line:
            total += 8
        elif 'A Z' in line:
            total += 3
        elif 'B X' in line:
            total += 1
        elif 'B Y' in line:
            total += 5
        elif 'B Z' in line:
            total += 9
        elif 'C X' in line:
            total += 7
        elif 'C Y' in line:
            total += 2
        elif 'C Z' in line:
            total += 6

print(f'total: {total}')

# A = Rock ... 1
# B = Paper ... 2
# C = Scissors ... 3
# X = Lose ... 0
# Y = Draw ... 3
# Z = Win ... 6

score = 0

with open(filename) as file_object:
    for line in file_object:
        if 'A X' in line:
            score += 3
        elif 'A Y' in line:
            score += 4
        elif 'A Z' in line:
            score += 8
        elif 'B X' in line:
            score += 1
        elif 'B Y' in line:
            score += 5
        elif 'B Z' in line:
            score += 9
        elif 'C X' in line:
            score += 2
        elif 'C Y' in line:
            score += 6
        elif 'C Z' in line:
            score += 7

print(f'score: {score}')
