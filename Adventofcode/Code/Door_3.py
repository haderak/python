filename = 'text_files/AoC3.txt'

rucksacks = {}
compartment_1_l = []
compartment_2_l = []
compartment_1 = {}
compartment_2 = {}
same_letters = []
alph_l = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
          'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
          'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
alph = {}

alph_index = 1

for letter in alph_l:
    alph[alph_index] = letter
    alph_index += 1

with open(filename, 'r') as file:
    file_length = len(file.readlines())

with open(filename) as file:
    i = 0
    for line in file:
        line = line.replace('\n', '')
        rucksacks[i] = line
        i += 1

j = 0

while j < file_length:
    compartment_1_l.append(rucksacks[j][:len(rucksacks[j]) // 2])
    compartment_2_l.append(rucksacks[j][len(rucksacks[j]) // 2:])
    j += 1

index = 0

for item in compartment_1_l:
    compartment_1[index] = [*item]
    index += 1

index = 0
for item in compartment_2_l:
    compartment_2[index] = [*item]
    index += 1

counter = 0

while counter < file_length:
    same_letters_l = list(set(compartment_1[counter]).intersection(set(compartment_2[counter])))
    same_letters += same_letters_l
    counter += 1

c = 0
same_letters_values = 0

testlist = ['b', 'c', 'K', 'i', 'r', 'A']

while c < file_length:
    for k, v in alph.items():
        if same_letters[c] == v:
            same_letters_values += k
    c += 1

print(same_letters_values)

cong0 = []
cong1 = []
cong2 = []
same_letters2 = []
same_letters3 = []

congruence_counter = 0

while congruence_counter < file_length:
    if congruence_counter % 3 == 0:
        cong0.append(rucksacks[congruence_counter])
    elif congruence_counter % 3 == 1:
        cong1.append(rucksacks[congruence_counter])
    elif congruence_counter % 3 == 2:
        cong2.append(rucksacks[congruence_counter])

    congruence_counter += 1

compare_counter = 0

while compare_counter < file_length / 3:
    same_letters_l2 = list(set(cong0[compare_counter]).intersection(set(cong1[compare_counter])))
    same_letters2 += same_letters_l2
    same_letters_l3 = list(set(same_letters2).intersection(set(cong2[compare_counter])))
    same_letters3 += same_letters_l3

    same_letters2 = []
    compare_counter += 1

c = 0
same_letters_values3 = 0

testlist = ['b', 'e', 'n', 'n', 'y']

while c < file_length / 3:
    for k, v in alph.items():
        if same_letters3[c] == v:
            same_letters_values3 += k
    c += 1

print(same_letters_values3)
