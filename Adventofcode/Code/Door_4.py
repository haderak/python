filename = 'text_files\AoC4.txt'

assigned_ids_raw = []
assigned_ids_dict = {}
split_ids = []
split_ids_converted = []
split_split_ids_dict = {}
overlap_number = []
r = {}
passing = []
covering_overlap = []
numbers = []

with open(filename) as file:
    for line in file:
        line = line.replace('\n', '')
        assigned_ids_raw.append(line)

i = 0
for number in assigned_ids_raw:
    assigned_ids_dict[i] = number.split(',')
    i += 1

i = 0
while i < len(assigned_ids_raw):
    split_ids.append(assigned_ids_dict[i][0])
    split_ids.append(assigned_ids_dict[i][1])
    i += 1

i = 0
for split_id in split_ids:
    split_split_ids_dict[i] = split_id.split('-')
    i += 1

i = 0
while i < len(split_split_ids_dict):
    for item in split_split_ids_dict[i]:
        numbers.append(item)
    i += 1

for i in range(0, len(numbers)):
    numbers[i] = int(numbers[i])

# print(f'Numbers: {numbers}')

i = 0
while i < len(numbers):
    a = range(numbers[i], numbers[i + 1] + 1)
    i += 2
    b = range(numbers[i], numbers[i + 1] + 1)
    overlap_number.append(list(set(a).intersection(b)))
    i += 2

i = 0
k = 0
while i < len(numbers):
    for n in range(numbers[i], numbers[i + 1] + 1):
        passing.append(n)
    r[k] = passing
    passing = []
    i += 2
    k += 1

# print(r)

i = 0
c = 0
while i < len(r):
    for overlap in overlap_number:
        if len(overlap) == len(r[i]):
            c += 1
        elif len(overlap) == len(r[i + 1]):
            c += 1
        i += 2

print(c)

cleared_overlap = list(filter(None, overlap_number))

print(cleared_overlap)
print(len(cleared_overlap))
