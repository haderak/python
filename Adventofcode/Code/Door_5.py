def retrieve_command(command_list, line_index):
    """Extract a single command from the list and splits it into its single words and puts them in a list"""
    split_command = command_list[line_index].split()
    return split_command


def command_information(split_command):
    """Spits out the integers of a single command"""
    nr_of_crates = int(split_command[1])
    old_stack = int(split_command[3])
    new_stack = int(split_command[5])
    command_information_list = [nr_of_crates, old_stack, new_stack]
    return command_information_list


def command(command_list):
    """Spits out a list with a list of commands"""
    commands = []
    for i in range(len(command_list)):
        split_command = retrieve_command(commands_list, i)
        x = command_information(split_command)
        commands.append(x)
    return commands


def remove_crate(data, nr_of_crates, old_stack):
    counter = 0
    for y in range(len(data)):
        crate = data[y][old_stack - 1]
        if crate == '[ ]':
            y += 1
        elif crate != '[ ]':
            if counter < nr_of_crates:
                temp_crate_storage.append(crate)
                data[y][old_stack - 1] = '[ ]'
                counter += 1
            else:
                break
    return data


def add_crate(data, new_stack):
    data.reverse()
    y = 0
    while y < len(data) + new_stack:
        for row in data:
            crate = row[new_stack - 1]
            if crate != '[ ]' and y >= len(data) - 1 and temp_crate_storage:
                data.insert(len(data), ['[ ]', '[ ]', '[ ]', '[ ]', '[ ]', '[ ]', '[ ]', '[ ]', '[ ]'])
                y = -1
            elif crate != '[ ]':
                y += 1
            elif crate == '[ ]':
                if temp_crate_storage:
                    row[new_stack - 1] = temp_crate_storage[0]
                    temp_crate_storage.pop(0)
                    y += 1
                else:
                    break
    data.reverse()
    return data


def run_code():
    k = 0
    while k < len(all_commands):
        remove_crate(stack_input, all_commands[k][0], all_commands[k][1])
        add_crate(stack_input, all_commands[k][2])
        k += 1

    return stack_input


def print_stacks(stacks):
    for stack in stacks:
        print(stack)


stack_input = [
    ['[ ]', '[ ]', '[ ]', '[ ]', '[V]', '[ ]', '[C]', '[ ]', '[M]'],  # 1
    ['[V]', '[ ]', '[J]', '[ ]', '[N]', '[ ]', '[H]', '[ ]', '[V]'],  # 2
    ['[R]', '[F]', '[N]', '[ ]', '[W]', '[ ]', '[Z]', '[ ]', '[N]'],  # 3
    ['[H]', '[R]', '[D]', '[ ]', '[Q]', '[M]', '[L]', '[ ]', '[R]'],  # 4
    ['[B]', '[C]', '[H]', '[V]', '[R]', '[C]', '[G]', '[ ]', '[R]'],  # 5
    ['[G]', '[G]', '[F]', '[S]', '[D]', '[H]', '[B]', '[R]', '[S]'],  # 6
    ['[D]', '[N]', '[S]', '[D]', '[H]', '[G]', '[J]', '[J]', '[G]'],  # 7
    ['[W]', '[J]', '[L]', '[J]', '[S]', '[P]', '[F]', '[S]', '[L]']  # 8
]

original_input = []

filename = 'text_files\AoC5.txt'
with open(filename) as file:
    for line in file:
        original_input.append(line)

commands_list = original_input[10:]
temp_crate_storage = []

all_commands = command(commands_list)

run_code()
print_stacks(stack_input)

stack_input = [
    ['[ ]', '[ ]', '[ ]', '[ ]', '[V]', '[ ]', '[C]', '[ ]', '[M]'],  # 1
    ['[V]', '[ ]', '[J]', '[ ]', '[N]', '[ ]', '[H]', '[ ]', '[V]'],  # 2
    ['[R]', '[F]', '[N]', '[ ]', '[W]', '[ ]', '[Z]', '[ ]', '[N]'],  # 3
    ['[H]', '[R]', '[D]', '[ ]', '[Q]', '[M]', '[L]', '[ ]', '[R]'],  # 4
    ['[B]', '[C]', '[H]', '[V]', '[R]', '[C]', '[G]', '[ ]', '[R]'],  # 5
    ['[G]', '[G]', '[F]', '[S]', '[D]', '[H]', '[B]', '[R]', '[S]'],  # 6
    ['[D]', '[N]', '[S]', '[D]', '[H]', '[G]', '[J]', '[J]', '[G]'],  # 7
    ['[W]', '[J]', '[L]', '[J]', '[S]', '[P]', '[F]', '[S]', '[L]']  # 8
]


def add_crate_reverse(data, new_stack):
    data.reverse()
    temp_crate_storage.reverse()
    y = 0
    while y < len(data) + new_stack:
        for row in data:
            crate = row[new_stack - 1]
            if crate != '[ ]' and y >= len(data) - 1 and temp_crate_storage:
                data.insert(len(data), ['[ ]', '[ ]', '[ ]', '[ ]', '[ ]', '[ ]', '[ ]', '[ ]', '[ ]'])
                y = -1
            elif crate != '[ ]':
                y += 1
            elif crate == '[ ]':
                if temp_crate_storage:
                    row[new_stack - 1] = temp_crate_storage[0]
                    temp_crate_storage.pop(0)
                    y += 1
                else:
                    break
    data.reverse()
    return data


def run_code_2():
    k = 0
    while k < len(all_commands):
        remove_crate(stack_input, all_commands[k][0], all_commands[k][1])
        add_crate_reverse(stack_input, all_commands[k][2])
        k += 1

    return stack_input


print('\n')

run_code_2()
print_stacks(stack_input)
