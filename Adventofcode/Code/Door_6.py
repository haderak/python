filename = 'text_files\AoC6.txt'

datastream = []
packets_of_four = []

with open(filename) as file:
    for line in file:
        datastream_string = line
    for char in datastream_string:
        datastream.append(char)


def packet_of_four():
    i = 0
    while i < len(datastream):
        packet = datastream[i: i + 4]
        packets_of_four.append(packet)
        i += 1
    return packets_of_four


def checking_duplicates(list_of_elements):
    if len(list_of_elements) == len(set(list_of_elements)):
        return False
    else:
        return True


def compare_strings(list_of_packets):
    i = 0
    while i < len(list_of_packets) - 4:
        if checking_duplicates(list_of_packets[i]):
            i += 1
        else:
            break
    return i


def calculate_number_of_chars(number_of_sets):
    number_of_chars = number_of_sets + 4
    print(number_of_chars)


def print_packets(list_to_print):
    for element in list_to_print:
        print(element)


packets = packet_of_four()
index = compare_strings(packets)
calculate_number_of_chars(index)

packets_of_fourteen = []


def packet_of_fourteen():
    i = 0
    while i < len(datastream) - 14:
        packet = datastream[i: i + 14]
        packets_of_fourteen.append(packet)
        i += 1
    return packets_of_fourteen


def calculate_number_of_chars(number_of_sets):
    number_of_chars = number_of_sets + 14
    print(number_of_chars)


packets2 = packet_of_fourteen()
index2 = compare_strings(packets2)
calculate_number_of_chars(index2)
