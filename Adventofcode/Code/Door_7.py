filename = 'text_files\AoC7.txt'

terminal_output = []
filesystem = {
    'start': [],
}

with open(filename) as file:
    i = 1
    for line in file:
        line = line.replace('\n', '')
        terminal_output.append(line)
        i += 1


def commands(output_list, file_dict):
    for output in output_list:
        if 'dir' in output:
            output = output.replace('dir ', '')
            for k in range(len(file_dict)):
                keys = list(file_dict.keys())
                if output == keys[k]:
                    key = keys[k]
                else:
                    key = output
                    file_dict[key] = []
    print(file_dict)


commands(terminal_output, filesystem)
