import numpy as np

filename = 'text_files/AoC8.txt'

forest = []

with open(filename) as file:
    treeline = []
    for line in file:
        line = line.replace('\n', '')
        for number in line:
            treeline.append(int(number))
        forest.append(treeline)
        treeline = []


def is_tree_taller(tree_line):
    x_1 = 0
    x_2 = 1
    visible_trees_in_line = []
    while x_2 < len(tree_line):
        tree_1 = tree_line[x_1]
        tree_2 = tree_line[x_2]
        if tree_2 > tree_1:
            visible_trees_in_line.append(x_2)
            x_1 = x_2
            x_2 += 1
        else:
            x_2 += 1
    return visible_trees_in_line


def turn_forest(frst):
    transp_forest = np.array(frst).T.tolist()
    return transp_forest


def visible_trees(frst):
    y = 0
    visible_trees_list = []
    while y < len(frst):
        visible_trees_list.append(is_tree_taller(frst[y]))
        y += 1
    return visible_trees_list


def create_coordinates_ltr(vis_trs):
    y = 0
    x = 0
    vis_trs_coordinates = []
    while y < len(vis_trs):
        if not vis_trs[y]:
            y += 1
        else:
            while x < len(vis_trs[y]):
                vis_trs_coordinates.append((vis_trs[y][x], y))
                x += 1
            x = 0
            y += 1
    return vis_trs_coordinates


def create_coordinates_rtl(vis_trs, frst):
    y = 0
    x = 0
    vis_trs_coordinates = []
    while y < len(vis_trs):
        if not vis_trs[y]:
            y += 1
        else:
            while x < len(vis_trs[y]):
                vis_trs_coordinates.append((len(frst[0]) - vis_trs[y][x] - 1, y))
                x += 1
            x = 0
            y += 1
    return vis_trs_coordinates


def swap_coordinates(list_of_coordinates):
    swapped_coordinates = [(sub[1], sub[0]) for sub in list_of_coordinates]
    return swapped_coordinates


def reverse_forest(frst):
    for tree_line in frst:
        tree_line.reverse()


def print_forest(frst):
    for tree_line in frst:
        print(tree_line)


def get_outer_coordinates(frst):
    outer_coordinates = []
    for y in range(len(frst)):
        outer_coordinates.append((0, y))
        outer_coordinates.append((len(frst[0]) - 1, y))
    for x in range(len(frst[0])):
        outer_coordinates.append((x, 0))
        outer_coordinates.append((x, len(frst) - 1))
    return outer_coordinates


def get_all_coordinates():
    outer_coordinates = get_outer_coordinates(forest)

    vis_trees_ltr = visible_trees(forest)
    coord_ltr = create_coordinates_ltr(vis_trees_ltr)

    transposed_forest = turn_forest(forest)
    vis_trees_ttb = visible_trees(transposed_forest)
    coord_ttb = create_coordinates_ltr(vis_trees_ttb)
    coord_ttb = swap_coordinates(coord_ttb)

    reverse_forest(transposed_forest)
    vis_trees_btt = visible_trees(transposed_forest)
    coord_btt = create_coordinates_rtl(vis_trees_btt, transposed_forest)
    coord_btt = swap_coordinates(coord_btt)

    reverse_forest(forest)
    vis_trees_rtl = visible_trees(forest)
    coord_rtl = create_coordinates_rtl(vis_trees_rtl, forest)

    all_coordinates = outer_coordinates + coord_ltr + coord_rtl + coord_ttb + coord_btt

    return all_coordinates


def weed_out_duplicates():
    single_coordinates = list(set(get_all_coordinates()))
    return single_coordinates


# Challenge 2

def can_i_see_this_tree(frst, coordinates):
    up_counters = []
    down_counters = []
    right_counters = []
    left_counters = []
    for coordinate in coordinates:
        x = coordinate[0]
        y = coordinate[1]
        tree = frst[y][x]
        counter_up = 0
        if y == 0:
            up_counters.append(counter_up)
        for y_up in range(y - 1, -1, -1):
            tree_up = frst[y_up][x]
            if tree > tree_up:
                counter_up += 1
                if y_up == 0:
                    up_counters.append(counter_up)
            elif tree == tree_up:
                counter_up += 1
                up_counters.append(counter_up)
                break
            elif tree < tree_up:
                counter_up += 1
                up_counters.append(counter_up)
                break

        counter_down = 0
        if y == len(frst) - 1:
            down_counters.append(counter_down)
        for y_down in range(y + 1, len(frst)):
            tree_down = frst[y_down][x]
            if tree > tree_down:
                counter_down += 1
                if y_down == len(frst) - 1:
                    down_counters.append(counter_down)
            elif tree == tree_down:
                counter_down += 1
                down_counters.append(counter_down)
                break
            elif tree < tree_down:
                counter_down += 1
                down_counters.append(counter_down)
                break

        counter_right = 0
        if x == len(frst[0]) - 1:
            right_counters.append(counter_right)
        for x_right in range(x + 1, len(frst[0])):
            tree_right = frst[y][x_right]
            if tree > tree_right:
                counter_right += 1
                if x_right == len(frst[0]) - 1:
                    right_counters.append(counter_right)
            elif tree == tree_right:
                counter_right += 1
                right_counters.append(counter_right)
                break
            elif tree < tree_right:
                counter_right += 1
                right_counters.append(counter_right)
                break

        counter_left = 0
        if x == 0:
            left_counters.append(counter_left)
        for x_left in range(x - 1, -1, -1):
            tree_left = frst[y][x_left]
            if tree > tree_left:
                counter_left += 1
                if x_left == 0:
                    left_counters.append(counter_left)
            elif tree == tree_left:
                counter_left += 1
                left_counters.append(counter_left)
                break
            elif tree < tree_left:
                counter_left += 1
                left_counters.append(counter_left)
                break
    counters = [up_counters, down_counters, left_counters, right_counters]
    return counters


def calculate_scenic_score(counters):
    scenic_scores = []
    for x in range(len(counters[0])):
        c_up = counters[0][x]
        c_down = counters[1][x]
        c_left = counters[2][x]
        c_right = counters[3][x]
        scenic_score = c_up * c_down * c_left * c_right
        scenic_scores.append(scenic_score)
    return scenic_scores


def main():
    singled_out_coordinates = weed_out_duplicates()
    print(f'weeded out coordinates: {singled_out_coordinates}')
    print(len(singled_out_coordinates))

    reverse_forest(forest)

    i_can_see_these_trees = can_i_see_this_tree(forest, singled_out_coordinates)
    scores = calculate_scenic_score(i_can_see_these_trees)
    scores.sort()
    print(scores[len(scores) - 1])


if __name__ == '__main__':
    main()
