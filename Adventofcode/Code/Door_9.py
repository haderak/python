from numpy import *

filename = 'text_files/AoC9.txt'

movements = []
momentary_h_position = array([0, 0])
momentary_t_position = array([0, 0])
h_movements = []
t_movements = [[0, 0]]
R = array([1, 0])
U = array([0, 1])
L = array([-1, 0])
D = array([0, -1])

with open(filename) as file:
    direction = []
    steps = []
    for line in file:
        line = line.replace(' ', '')
        direction.append(line[0])
        steps.append(int(line[1:len(line)]))


def move_h(mvmt, mmtry_h_ps):
    if mvmt == 'R':
        mmtry_h_ps += R
    elif mvmt == 'U':
        mmtry_h_ps += U
    elif mvmt == 'L':
        mmtry_h_ps += L
    elif mvmt == 'D':
        mmtry_h_ps += D
    h_movements.append(list(mmtry_h_ps))
    return mmtry_h_ps


def get_space_h_t(mmtry_h_pos, mmtry_t_pos):
    h_t_space = mmtry_h_pos - mmtry_t_pos
    return h_t_space


def move_t(mvmt, h_t_space, mmtry_t_pos):
    if linalg.norm(h_t_space) == sqrt(2) or linalg.norm(h_t_space) == 1 or linalg.norm(h_t_space) == 0:
        mmtry_t_pos += array([0, 0])
    elif linalg.norm(h_t_space) == 2:
        if mvmt == 'R':
            mmtry_t_pos += R
        elif mvmt == 'U':
            mmtry_t_pos += U
        elif mvmt == 'L':
            mmtry_t_pos += L
        elif mvmt == 'D':
            mmtry_t_pos += D
    elif linalg.norm(h_t_space) > sqrt(2):
        if array_equal(h_t_space, array([2, 1])):
            mmtry_t_pos += h_t_space + L
        elif array_equal(h_t_space, array([2, -1])):
            mmtry_t_pos += h_t_space + L
        elif array_equal(h_t_space, array([-2, 1])):
            mmtry_t_pos += h_t_space + R
        elif array_equal(h_t_space, array([-2, -1])):
            mmtry_t_pos += h_t_space + R
        elif array_equal(h_t_space, array([1, 2])):
            mmtry_t_pos += h_t_space + D
        elif array_equal(h_t_space, array([-1, 2])):
            mmtry_t_pos += h_t_space + D
        elif array_equal(h_t_space, array([1, -2])):
            mmtry_t_pos += h_t_space + U
        elif array_equal(h_t_space, array([-1, -2])):
            mmtry_t_pos += h_t_space + U
    t_movements.append(list(mmtry_t_pos))
    return mmtry_t_pos


def move():
    for mvmts in range(len(steps)):
        move_dir = direction[mvmts]
        move_number = steps[mvmts]
        for mvmt in range(move_number):
            if array_equal(momentary_h_position, momentary_t_position):
                move_h(move_dir, momentary_h_position)
            else:
                move_h(move_dir, momentary_h_position)

                h_t_space = get_space_h_t(momentary_h_position, momentary_t_position)
                move_t(move_dir, h_t_space, momentary_t_position)

    return momentary_h_position, momentary_t_position, h_movements, t_movements


def get_number_of_positions(t_mvmts):
    check_movements = t_mvmts
    no_duplicates = []
    for vec in check_movements:
        if vec not in no_duplicates:
            no_duplicates.append(vec)
        else:
            continue
    return no_duplicates


def main():
    move()
    coordinates = get_number_of_positions(t_movements)
    print(coordinates)
    print(len(coordinates))


if __name__ == '__main__':
    main()
