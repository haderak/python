from numpy import *

filename = 'text_files/Test_2.txt'

momentary_h_position = array([0, 0])
momentary_1_position = array([0, 0])
momentary_2_position = array([0, 0])
momentary_3_position = array([0, 0])
momentary_4_position = array([0, 0])
momentary_5_position = array([0, 0])
momentary_6_position = array([0, 0])
momentary_7_position = array([0, 0])
momentary_8_position = array([0, 0])
momentary_9_position = array([0, 0])
h_movements = []
t_movements = [[0, 0]]
R = array([1, 0])
U = array([0, 1])
L = array([-1, 0])
D = array([0, -1])

with open(filename) as file:
    direction = []
    steps = []
    for line in file:
        line = line.replace(' ', '')
        direction.append(line[0])
        steps.append(int(line[1:len(line)]))


def move_h(mvmt, mmtry_h_ps):
    if mvmt == 'R':
        mmtry_h_ps += R
    elif mvmt == 'U':
        mmtry_h_ps += U
    elif mvmt == 'L':
        mmtry_h_ps += L
    elif mvmt == 'D':
        mmtry_h_ps += D
    h_movements.append(list(mmtry_h_ps))
    return mmtry_h_ps


def get_space_h_t(mmtry_h_pos, mmtry_t_pos):
    h_t_space = mmtry_h_pos - mmtry_t_pos
    return h_t_space


def main():
    move()
    coordinates = get_number_of_positions(t_movements)
    print(coordinates)
    print(len(coordinates))


if __name__ == '__main__':
    main()
