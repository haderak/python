import json
import pandas as pd

data = {
    'Benjamin Lux': {
        '19.05.2023': 0,
        '20.05.2023': 1,
        '22.05.2023': -1,
        '23.05.2023': 2,
    },
    'Kira Schmitz': {
        '19.05.2023': 1,
        '21.05.2023': 3,
        '22.05.2023': 1,
        '23.05.2023': 0,
    },
    'Florian Lux': {
        '18.05.2023': 1,
        '19.05.2023': 2,
        '22.05.2023': -2,
    },
    'Tanja Lux': {
        '20.05.2023': 1,
        '21.05.2023': -1,
        '23.05.2023': 2,
    },
}

with open('data.json', 'w') as file:
    json.dump(data, file)

new_data = {
    'Tanja Lux': {
        '19.05.2023': 2,
        '18.05.2023': 1,
    },
    'Bettina Schwarz': {
        '20.05.2023': 1,
        '21.05.2023': 2,
        '22.05.2023': -1,
    },
    'Benjamin Lux': {
        '28.05.2023': 5,
        '18.05.2023': 1
    }
}

for name, new_date_dict in new_data.items():
    if name in data:
        for new_date, new_value in new_data[name].items():
            data[name][new_date] = new_value
    else:
        data[name] = new_date_dict

with open('data.json', 'r') as file:
    json.load(file)

df = pd.DataFrame.from_dict(data).T

df.to_excel('data.xlsx')
