def fibIterativ(k):
    def fib(a, b, n):
        if n <= 0:
            return a
        else:
            return fib(b, a + b, n - 1)
    return fib(0,1,k)


if __name__ == '__main__':
    print(fibIterativ(2))
