import math


class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def s(self):
        return (self.a + self.b + self.c) / 2

    def h_c(self):
        return 2 * math.sqrt(self.s() * (self.s() - self.a) * (self.s() - self.b) * (self.s() - self.c)) / self.c

    def area(self):
        return self.c * self.h_c() / 2


class EqTriangle(Triangle):
    def __init__(self, c):
        super().__init__(c, c, c)


class Hexagon(EqTriangle):
    def hex_area(self):
        triangle_area = super().area()
        return triangle_area * 6


class Pyramid(Hexagon):
    def __init__(self, c, h):
        super().__init__(c)
        self.h = h

    def volume(self):
        return self.hex_area() * self.h / 3