from Player import Triangle
from Player import EqTriangle
from Player import Hexagon
from Player import Pyramid

triangle = Triangle(2, 3, 4)
eqtriangle = EqTriangle(3)
hexagon = Hexagon(3)
pyramid = Pyramid(3, 5)

if __name__ == '__main__':
    print("Triangle:")
    print(triangle.s())
    print(triangle.h_c())
    print(triangle.area())

    print("\nEqTriangle:")
    print(eqtriangle.s())
    print(eqtriangle.h_c())
    print(eqtriangle.area())

    print("\nHexagon:")
    print(hexagon.hex_area())

    print("\nPyramid")
    print(pyramid.volume())
