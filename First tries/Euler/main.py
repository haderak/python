def factorial(n):
    if n < 0:
        return None
    if n == 0:
        return 1
    result = 1
    for i in range(1, n + 1):
        result = result * i

    return result


def e(n):
    result = 0
    for i in range(n + 1):
        result = result + 1 / factorial(i)
    return result


def exp(n):
    result = pow(e(1000), n)
    return result

n = input("n = ")
print(exp(n))

input("Press Enter")
