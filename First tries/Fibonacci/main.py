allSaved = {}


def fibonacci(n):
    if n <= 0:
        return None
    if n == 1 or n == 2:
        return 1
    result = fibonacci(n - 1) + fibonacci(n - 2)

    return result


def fib(n):
    if n <= 0:
        return None
    if n == 1 or n == 2:
        return 1
    saved = allSaved.get(n)
    if saved is not None:
        return saved
    else:
        result = fib(n - 1) + fib(n - 2)
        allSaved[n] = result
    return result


#for n in range(1000):
 #   print(n, ": ", fib(n))


def gold(n):
    result = fib(n)/fib(n-1)

    return result

print("Please enter the index of the Fibonacci number you want to know.")
num = input("n = ")

print("The Fibonacci number with index-number " + num + " is:")
print(fib(int(num)))

print("The golden ration is therefore approximately:")
print(gold(int(num)))

input("Press any key to exit")