alphabet = "abcdefghijklmnopqrstuvwxyzäöü"
gallows = ["",
           "==========",
           "    ||\n    ||\n    ||\n    ||\n    ||\n    ||",
           "    +===========+",
           "    ||/\n    ||\n    ||\n    ||\n    ||\n    ||",
           "    ||/         |\n    ||          |\n    ||\n    ||\n    ||\n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||\n    ||\n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||          |\n    ||\n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||          |\n    ||         /\n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||          |\n    ||         / \\ \n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||         /|\n    ||         / \\ \n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||         /|\\\n    ||         / \\ \n    ||"]


def print_gallows(k):
    if k == 0:
        print(gallows[0])
    if k == 1:
        print(gallows[1])
    if k == 2:
        print(gallows[2])
        print(gallows[1])
    if k == 3:
        print(gallows[3])
        print(gallows[2])
        print(gallows[1])
    if k > 3:
        print(gallows[3])
        print(gallows[k])
        print(gallows[1])


def get_index_positions(list_of_elems, element):
    index_pos_list = []
    index_pos = 0
    while True:
        try:
            # Search for item in list from indexPos to the end of list
            index_pos = list_of_elems.index(element, index_pos)
            # Add the index position in list
            index_pos_list.append(index_pos)
            index_pos += 1
        except ValueError as e:
            break
    return index_pos_list


def print_guess(guess):
    guess_with_spaces = ""
    for char in guess:
        guess_with_spaces += char + " "
    print(guess_with_spaces)


def main():
    is_invalid_word = True
    while is_invalid_word:
        word = input("Type in a word: ").lower()

        for char in word:
            if char not in alphabet:
                print("Character", char, "not allowed. Only these ones are allowed:", alphabet)
                break
        else:
            is_invalid_word = False

    used_letters = []

    counter = 0

    k = 0

    guess = ""

    while counter < len(word):
        guess = guess + "_"
        counter += 1

    print_guess(guess)
    print("")

    while guess != word:
        i = 0

        is_invalid_letter = True

        while is_invalid_letter:
            letter = input("Type in a letter: ").lower()

            if letter not in alphabet:
                print(letter, "is not allowed. Only these are allowed:", alphabet)

            else:
                is_invalid_letter = False

        if len(letter) != 1:
            print("Please only type in one letter at a time!")
            continue

        num_letter = get_index_positions(word, letter)

        if letter in used_letters:
            print("")
            print("This letter was already used!")
            continue

        used_letters.append(letter)

        if letter in word:
            while i < len(word):
                if i in num_letter:
                    guess = guess[0:num_letter[0]] + letter + guess[num_letter[0] + 1:]
                    num_letter.pop(0)
                i += 1
        if letter not in word:
            k += 1
            if k == 11:
                break

        print("")
        print_guess(guess)
        print("")
        print("Used letters: ")
        print(used_letters, "\n")
        print_gallows(k)
        print("")

    if k == 11:
        print("")
        print_guess(guess)
        print("")
        print("Used letters: ")
        print(used_letters, "\n")
        print_gallows(k)
        print("\nGame Over!")
        input("Press any key to exit.")

    else:
        print_guess(guess)
        print("\nCongratulations you won!\n")
        input("Press any key to exit.")


if __name__ == "__main__":
    main()
