alphabet = "abcdefghijklmnopqrstuvwxyzäöü"
gallows = ["",
           "==========",
           "    ||\n    ||\n    ||\n    ||\n    ||\n    ||",
           "    +===========+",
           "    ||/\n    ||\n    ||\n    ||\n    ||\n    ||",
           "    ||/         |\n    ||          |\n    ||\n    ||\n    ||\n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||\n    ||\n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||          |\n    ||\n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||          |\n    ||         /\n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||          |\n    ||         / \\ \n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||         /|\n    ||         / \\ \n    ||",
           "    ||/         |\n    ||          |\n    ||          0\n    ||         /|\\\n    ||         / \\ \n    ||"]


def print_gallows(k):
    if k == 0:
        print(gallows[0])
    elif k == 1:
        print(gallows[1])
    elif k == 2:
        print(gallows[2])
        print(gallows[1])
    elif k == 3:
        print(gallows[3])
        print(gallows[2])
        print(gallows[1])
    else:
        print(gallows[3])
        print(gallows[k])
        print(gallows[1])


def get_valid_word():
    while True:
        word = input("Type in a word: ").lower()

        for char in word:
            if char not in alphabet:
                print("Character", char, "not allowed. Only these ones are allowed:", alphabet)
                break
        else:
            return word


def get_valid_letter(used_letters):
    while True:
        letter = input("Type in a letter: ").lower()

        if len(letter) != 1:
            print("Please only type in one letter at a time!")

        elif letter in used_letters:
            print()
            print("This letter was already used!")

        elif letter not in alphabet:
            print(letter, "is not allowed. Only these are allowed:", alphabet)

        else:
            return letter


def get_index_positions(list_of_elems, element):
    index_pos_list = []
    index_pos = 0
    while True:
        try:
            # Search for item in list from indexPos to the end of list
            index_pos = list_of_elems.index(element, index_pos)
            # Add the index position in list
            index_pos_list.append(index_pos)
            index_pos += 1
        except ValueError as e:
            break
    return index_pos_list


def print_guess(guess):
    guess_with_spaces = ""
    for char in guess:
        guess_with_spaces += char + " "
    print(guess_with_spaces)


def main():
    word = get_valid_word()

    for i in range(50):
        print()

    used_letters = []

    counter = 0

    k = 0

    guess = ""

    while counter < len(word):
        guess = guess + "_"
        counter += 1

    print_guess(guess)
    print()

    while guess != word:

        letter = get_valid_letter(used_letters)

        used_letters.append(letter)

        num_letter = get_index_positions(word, letter)

        if letter in word:
            for i in num_letter:
                guess = guess[0:i] + letter + guess[i + 1:]
        else:
            k += 1

        print()
        print_guess(guess)
        print()
        print("Used letters: ")
        print(used_letters, "\n")
        print_gallows(k)

        if k != 11:
            print("Tries left:", 11 - k)
            print()
        else:
            print("\nGame Over!")
            input("Press any key to exit.")
            exit(0)

    else:
        print_guess(guess)
        print("\nCongratulations you won!\n")
        input("Press any key to exit.")


if __name__ == "__main__":
    main()
