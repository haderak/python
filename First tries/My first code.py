
name = input("Enter your name: ")
last_name = input("Enter your last name: ")
birthday = input("Enter your date of birth (DD.MM.YYYY): ")
age = input("Enter your age: ")
age2 = int(age) + 10


print("Hello, " + name + " " + last_name)
print("Your birthday is the " + birthday)
print("You are currently " + age + " years old.")
print("In 10 years you will be {} years old.".format(age2))
input("Press Enter to continue.")