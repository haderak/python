allSaved = {}


def main():
    var1 = input("n = ")
    var2 = input("k = ")
    print(binomial2(int(var1), int(var2)))
    input("Press Enter to continue!")


def binomial2(n, k):
    string = "{},{}".format(n, k)
    saved = allSaved.get(string)
    if saved is not None:
        return saved
    if n < k:
        return None
    if n == k or k == 0:
        result = 1
    elif k == 1:
        result = n
    else:
        result = binomial2(n - 1, k - 1) + binomial2(n - 1, k)

    allSaved[string] = result
    return result


if __name__ == '__main__':
    main()
