class Weapon:
    all_weapons = []
    kills = None

    def __init__(self, name, abbreviation):
        self.name = name
        self.abbreviation = abbreviation
        Weapon.all_weapons.append(self)

    def set_kills(self, kills):
        self.kills = kills

    def __str__(self):
        return self.name + " (" + self.abbreviation + ")"

    def does_kill(self, other_weapon):
        return other_weapon in self.kills

    def get_defeating_sentence(self, other_weapon):
        return self.name + " " + self.kills.get(other_weapon) + " " + other_weapon.name

    @classmethod
    def get_weapon(cls, abrv: str):
        for w in Weapon.all_weapons:
            if w.abbreviation == abrv:
                return w
        return None
