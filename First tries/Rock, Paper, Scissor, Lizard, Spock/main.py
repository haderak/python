import random
from Weapon import Weapon

rock = Weapon("Rock", "r")
paper = Weapon("Paper", "p")
scissors = Weapon("Scissors", "s")
lizard = Weapon("Lizard", "l")
spock = Weapon("Spock", "o")

rock.set_kills({scissors: "crushes", lizard: "crushes"})
paper.set_kills({rock: "covers", spock: "disproves"})
scissors.set_kills({paper: "cuts", lizard: "decapitates"})
lizard.set_kills({spock: "poisons", paper: "eats"})
spock.set_kills({rock: "vaporises", scissors: "smashes"})


def print_list(list):
    result = ""
    for entry in list:
        result = result + " " + str(entry)

    return result


def rock_paper_scissor_lizard_spock():
    plyr = input("Type in" + print_list(Weapon.all_weapons) + ": ")
    plyr_weapon: Weapon = Weapon.get_weapon(plyr)
    comp_weapon: Weapon = random.choice(Weapon.all_weapons)

    if plyr_weapon is None:
        print("Not a valid selection!\n")
        return rock_paper_scissor_lizard_spock()
    else:
        print("Player: ", plyr_weapon.name)
        print("Computer: ", comp_weapon.name)
        if plyr_weapon == comp_weapon:
            print("Draw!")
        elif plyr_weapon.does_kill(comp_weapon):
            print(plyr_weapon.get_defeating_sentence(comp_weapon))
            print("Player wins!")
        else:
            print(comp_weapon.get_defeating_sentence(plyr_weapon))
            print("Computer wins!")

    end = input("\nPress any key to exit or r to replay: ")

    if end == "r":
        print("")
        return rock_paper_scissor_lizard_spock()
    else:
        exit()


def main():
    rock_paper_scissor_lizard_spock()


if __name__ == '__main__':
    main()
