import random


def rock_paper_scissor():
    possibilities = ["r", "p", "s"]
    plyr = input("Type in Rock (r), Paper (p) or Scissors (s): ")

    if plyr not in possibilities:
        print("Type in r, p or s! \n")
        return rock_paper_scissor()
    else:
        comp = random.choice(possibilities)

    print("Player: ", plyr)
    print("Computer: ", comp)

    if comp == plyr:
        print("Draw! \n")

    elif plyr == "r":
        if comp == "p":
            print("Computer wins! \n")
        if comp == "s":
            print("Player wins! \n")

    elif plyr == "p":
        if comp == "s":
            print("Computer wins! \n")
        if comp == "r":
            print("Player wins! \n")

    elif plyr == "s":
        if comp == "r":
            print("Computer wins! \n")
        if comp == "p":
            print("Player wins! \n")

    end = input("Press any key to exit or r to replay: ")

    if end == "r":
        print("")
        return rock_paper_scissor()
    else:
        exit()


def main():
    rock_paper_scissor()


if __name__ == '__main__':
    main()
