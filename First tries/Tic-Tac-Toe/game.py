from player import Computer, HumanPlayer


class TicTacToe:
    def __init__(self):
        self.board = self.make_board
        current_winner = False

    @staticmethod
    def make_board():
        return [" " for _ in range(9)]

    def print_board(self):
        for row in [self.board[i * 3: (i + 1) * 3] for i in range(3)]:
            print(" | ".join(row))

    @staticmethod
    def board_nums():
        for row in [[str(i) for i in range(j * 3, (j + 1) * 3)] for j in range(3)]:
            print(" | ".join(row))

    def available_moves(self):
        return [i for i, x in enumerate(self.board()) if x == " "]

    def make_move(self, square, letter):
        if self.board[square] == " "
            self.board[square] == letter


class Play:
    def __init__(self):




if __name__ == "__maine__":
    x_player = HumanPlayer("X")
    o_player = Computer("O")
    t = TicTacToe()
