hints = ["It lives in Africa and Asia.", "It is grey and lives in herds.", "It has big ears.", "It has a trunk.",
         "The animal was the elephant!"]
hint_number = {
    0: "First",
    1: "Second",
    2: "Third",
    3: "Fourth"
}

secret_word = "elephant"
guess = ""
guess_count = 0

print("Guess the Animal!\n")

while guess != secret_word:
    print("\n", hint_number[guess_count] + " hint:")
    print(hints[guess_count])
    guess = input("Put in your guess: ")
    guess_count += 1

    if guess_count == 4:
        break

if guess == secret_word:
    print("You win!")
else:
    print()
    print("Game Over!")

input("Press any key to exit.")
