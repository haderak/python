def divide(dividend, divisor):
    if divisor == 0:
        raise ZeroDivisionError
    return dividend / divisor


def multiply(factor1, factor2):
    if factor2 == 0:
        return 0
    result = 0
    counter = 0
    while counter < abs(factor2):
        result = result + factor1
        counter = counter + 1

    if factor2 < 0:
        result = result * -1

    return result


def multiply2(factor1, factor2):
    if factor2 == 0:
        return 0

    result = 0

    for i in range(abs(factor2)):
        result = result + factor1

    if factor2 < 0:
        result = result * -1

    return result


def factorial(n):
    if n < 0:
        return None
    if n == 0:
        return 1
    result = 1
    for i in range(1, n + 1):
        result = result * i

    return result


def factorial2(n):
    if n < 0:
        return None
    if n == 0:
        return 1
    return n * factorial2(n - 1)


def factorial3(n):
    return None if n < 0 else 1 if n == 0 else n * factorial3(n - 1)


def binomial(n, k):
    if n < k:
        return None
    if n == k:
        return 1
    if k == 1:
        return n

    return factorial(n) / (factorial(k) * factorial(n - k))


allSaved = {}


def binomial2(n, k):
    string = "{},{}".format(n, k)
    saved = allSaved.get(string)
    if saved is not None:
        return saved
    if n < k:
        return None
    if n == k or k == 0:
        result = 1
    elif k == 1:
        result = n
    else:
        result = binomial2(n - 1, k - 1) + binomial2(n - 1, k)

    allSaved[string] = result
    return result


def binomial3(n, k):
    if n < k:
        return None
    if n == k or k == 0:
        return 1
    if k == 1:
        return n

    return binomial3(n - 1, k - 1) + binomial3(n - 1, k)


n = 7
k = 3
print(binomial(n, k))
print(binomial2(n, k))
print(binomial3(n, k))
