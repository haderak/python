import customtkinter as ctk
import pandas as pd
from datetime import datetime

ctk.set_appearance_mode("dark")
ctk.set_default_color_theme("dark-blue")
ctk.deactivate_automatic_dpi_awareness()

current_datetime = datetime.now().strftime("%Y%m%d")
current_day_month = datetime.now().strftime("%d%m")
current_month = datetime.now().strftime("%m")

str_current_datetime = str(current_datetime)

student_label_fg_color = "#1194ee"  # blue
student_label_absence_color = "#f30c05"  # red
student_label_called_color = "#F96A06"  # orange

minus_button_fg_color = "#F68EFF"  # pink
minus_button_hover_color = "#ac00bb"  # darker pink

plus_button_fg_color = "#48CE3D"  # green
plus_button_hover_color = "#258c1c"  # darker green

grayed_out_color = "#808080"  # gray


class Window(ctk.CTk):
    def __init__(self):
        super().__init__()

        self.title("Mitarbeit 1.1")
        self.geometry("1200x400")
        self.iconbitmap("C:\Python\GUI\Fifth\education.ico")
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)

        # List of student names
        self.name_list = []
        self.create_name_list()

        # List of the student objects
        self.student_list = []

        # Dictionary of the students Mitarbeit
        self.student_mitarbeit_dict = {}

        # Path for the Excel file of the Mitarbeit (adds the current date to the file's name)
        self.excel_file_path = rf"C:\Python\GUI\Fifth\{str_current_datetime}-Mitarbeit.xlsx"

        # A save button to append the students Mitarbeit to a list in the Mitarbeit-dictionary
        self.save_button = ctk.CTkButton(self, text='Speichern', command=self.append_mitarbeit_list_to_dict)
        self.save_button.grid(row=0, column=1)

        # Creates an Excel-file for the Mitarbeit from the Mitarbeit-dictionary
        self.export_button = ctk.CTkButton(self, text='Exportieren', command=self.save_as_excel)
        self.export_button.grid(row=1, column=1)

        # Scroll-frame object
        self.scroll_frame = ScrollFrame(self, fg_color="black")
        self.scroll_frame.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")

        y = 0
        # m = math.floor(self.winfo_screenwidth() // 256)
        # if m >= 7:
        #     m = 6

        for x, student in enumerate(self.name_list):
            # creates the student frame
            self.student = Widgets(self.scroll_frame,
                                   student_name=student)
            if x % 4 == 0 and x != 0:
                y += 1
            self.student.grid(row=y, column=x % 4, pady=5)

            self.student_list.append(self.student)

    def create_name_list(self):
        """Create the names of the individual students from an Excel list"""
        student_names = pd.read_excel(
            r"C:\Users\BenjaminLux\OneDrive - BG Perchtoldsdorf\!Schule\!Mathematik\2. Klasse\2B\Klassenliste 2B.xlsx",
            sheet_name="100 Aktive Schüler")
        family_names = student_names["Familienname"].tolist()
        first_names = student_names["Vorname"].tolist()

        for family_name, first_name in zip(family_names, first_names):
            self.name_list.append(f"{family_name} {first_name}")

    def append_mitarbeit_list_to_dict(self):
        """Adds the Mitarbeit-list of the students to a dict with the name as key and the list as value"""
        for student in self.student_list:
            if student.name_label.cget('text') not in self.student_mitarbeit_dict:
                self.student_mitarbeit_dict[student.name_label.cget('text')] = []
            self.student_mitarbeit_dict[student.name_label.cget('text')].append(student.mitarbeit.get())
        print(self.student_mitarbeit_dict)

    def save_as_excel(self):
        """Saves the Mitarbeit of the students to an Excel-file"""
        df = pd.DataFrame(data=self.student_mitarbeit_dict).T
        # df = pd.DataFrame.from_dict(self.student_mitarbeit_dict)

        with pd.ExcelWriter(self.excel_file_path) as writer:
            df.to_excel(writer, sheet_name='Mitarbeit', index=False)


class ScrollFrame(ctk.CTkScrollableFrame):
    """Creates a scrollable frame as a master for the individual student frames"""

    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)

        self.grid_columnconfigure((0, 1, 2, 3, 4, 5), weight=1)
        self.grid_rowconfigure((0, 1, 2, 3, 4, 5, 6), weight=1)


class Widgets(ctk.CTkFrame):
    """A class for all the widgets in a student-frame"""

    def __init__(self, master, student_name, **kwargs):
        super().__init__(master, **kwargs)

        self.grid_columnconfigure((0, 2), weight=1)
        self.grid_rowconfigure((0, 1, 2), weight=1)
        self.grid_columnconfigure(1, weight=2)

        self.student_name = student_name
        self.mitarbeit = ctk.IntVar(value=0)

        # Name of student label
        self.name_label = ctk.CTkLabel(self, text=student_name,
                                       text_color="black",
                                       fg_color=student_label_fg_color,
                                       corner_radius=6,
                                       font=("Verdana", 14, "bold"))
        self.name_label.grid(row=0,
                             column=0,
                             padx=10,
                             pady=10,
                             sticky="w",
                             columnspan=3)

        # Attendance switch
        self.attendance_switch = ctk.CTkSwitch(self,
                                               text="Anwesend",
                                               progress_color="red",
                                               fg_color="green",
                                               command=self.absent_student)
        self.attendance_switch.grid(row=1,
                                    column=0,
                                    padx=10,
                                    pady=10,
                                    sticky="w")

        # Drangenommen checkbox
        self.called_checkbox = ctk.CTkCheckBox(self,
                                               text="Dran-\ngenommen?",
                                               command=self.called_student)
        self.called_checkbox.grid(row=1,
                                  column=1,
                                  padx=10,
                                  pady=10,
                                  sticky="w",
                                  columnspan=2)

        # Mitarbeit
        self.minus_button = ctk.CTkButton(self,
                                          text="-",
                                          width=90,
                                          text_color="black",
                                          fg_color=minus_button_fg_color,
                                          hover_color=minus_button_hover_color,
                                          command=self.minus,
                                          corner_radius=6)
        self.minus_button.grid(row=2,
                               column=0,
                               sticky="w")

        self.mitarbeit_label = ctk.CTkLabel(self,
                                            textvariable=str(self.mitarbeit),
                                            width=46,
                                            fg_color="gray30",
                                            corner_radius=6)
        self.mitarbeit_label.place(relx=0.5,
                                   rely=.89,
                                   anchor=ctk.CENTER)

        self.plus_button = ctk.CTkButton(self,
                                         text="+",
                                         text_color="black",
                                         width=90,
                                         fg_color=plus_button_fg_color,
                                         hover_color=plus_button_hover_color,
                                         command=self.plus,
                                         corner_radius=6)
        self.plus_button.place(relx=.8, rely=.89, anchor=ctk.CENTER)
        self.plus_button.grid(row=2,
                              column=2,
                              sticky="e")

    def plus(self):
        """Adds 1 to the IntVar"""
        self.mitarbeit.set(self.mitarbeit.get() + 1)

    def minus(self):
        """Subtracts 1 from IntVar"""
        self.mitarbeit.set(self.mitarbeit.get() - 1)

    def absent_student(self):
        """Changes the color of the name tag and the buttons
        + deactivates the buttons, when the attendance switch is turned on.
        If the switch is turned off, it resets"""

        if self.attendance_switch.get() == 1:
            self.name_label.configure(True,
                                      fg_color=student_label_absence_color)  # red

            self.minus_button.configure(True,
                                        fg_color=grayed_out_color,
                                        hover_color=grayed_out_color,
                                        command=None)  # gray

            self.plus_button.configure(True,
                                       fg_color=grayed_out_color,
                                       hover_color=grayed_out_color,
                                       command=None)  # gray

            self.called_checkbox.destroy()

        elif self.attendance_switch.get() == 0:
            self.name_label.configure(True,
                                      fg_color=student_label_fg_color)  # blue

            self.minus_button.configure(True,
                                        fg_color=minus_button_fg_color,
                                        hover_color=minus_button_hover_color,
                                        command=self.minus)  # normal colors

            self.plus_button.configure(True,
                                       fg_color=plus_button_fg_color,
                                       hover_color=plus_button_hover_color,
                                       command=self.plus)  # normal colors

            self.called_checkbox = ctk.CTkCheckBox(self,
                                                   text="Dran-\ngenommen?",
                                                   command=self.called_student)
            self.called_checkbox.grid(row=1,
                                      column=1,
                                      padx=10,
                                      pady=10,
                                      sticky="w",
                                      columnspan=2)

    def called_student(self):
        """Changes the student's name label when checkbox 'Drangenommen' is checked or unchecked"""
        if self.called_checkbox.get() == 1:
            self.name_label.configure(True,
                                      text_color="white",
                                      fg_color=student_label_called_color)
        else:
            self.name_label.configure(True,
                                      text_color="black",
                                      fg_color=student_label_fg_color)


window = Window()
window.mainloop()
