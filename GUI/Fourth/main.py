import customtkinter as ctk
import math

ctk.set_appearance_mode("system")
ctk.set_default_color_theme("dark-blue")
ctk.deactivate_automatic_dpi_awareness()


class Frame(ctk.CTkFrame):
    def __init__(self, master, value):
        super().__init__(master)

        self.grid_columnconfigure((0, 1, 2), weight=1)
        self.value = value
        self.mitarbeit = ctk.IntVar(value=0)
        self.name_tag = "#1194ee"

        self.name = ctk.CTkLabel(self, text=value, text_color="black", fg_color=self.name_tag, corner_radius=6)
        self.name.grid(row=0, column=0, padx=0, pady=5, sticky="w", columnspan=3)

        self.switch = ctk.CTkSwitch(self, text="Da?", progress_color="red", fg_color="green")
        self.switch.grid(row=1, column=0, padx=5, pady=5, sticky="w")

        self.checkbox = ctk.CTkCheckBox(self, text="Auf-\ngerufen")
        self.checkbox.grid(row=1, column=1, padx=5, pady=5, sticky="n", columnspan=2)

        self.minus_button = ctk.CTkButton(self, text="-", width=75, text_color="black", fg_color="#F68EFF",
                                          hover_color="#ac00bb",
                                          command=self.minus)
        self.minus_button.grid(row=2, column=0, padx=0, pady=5, sticky="w")

        self.display = ctk.CTkLabel(self, textvariable=str(self.mitarbeit), width=25, fg_color="gray30")
        self.display.place(relx=0.41, rely=.72)

        self.plus_button = ctk.CTkButton(self, text="+", text_color="black", width=75, fg_color="#48CE3D",
                                         hover_color="#258c1c",
                                         command=self.plus)
        self.plus_button.grid(row=2, column=2, padx=0, pady=5, sticky="w")

    def plus(self):
        self.mitarbeit.set(self.mitarbeit.get() + 1)

    def minus(self):
        self.mitarbeit.set(self.mitarbeit.get() - 1)

    def absence(self, absence_switch):
        if absence_switch == 0:
            self.name_tag = "#999999"
            self.name.after(50, self.absence)


class App(ctk.CTk):
    def __init__(self):
        super().__init__()

        self.title("Mitarbeit 1.0")
        self.window_width = (self.winfo_screenwidth() * 0.5)
        self.geometry(f"1024x300")
        self.minsize(290, 400)
        self.resizable(True, True)
        self.grid_columnconfigure((0, 1, 2, 3, 4), weight=1)
        self.grid_rowconfigure((0, 1, 2, 3, 4, 6, 7, 8), weight=1)
        self.student_list = ["Dostal Elias", "Eichelberger Magdalena", "Fischer Von Hoepfner Sebastian",
                             "Fleischhacker Letizia", "Fleischhacker Luzia", "Goethel Felix", "Göller Jonathan",
                             "Gönitzer Clemens", "Haensel Jona", "Heyny Ben", "Hobusch Arno", "Huber Maximilian",
                             "Kimla Franziska", "Kummer Adam", "Machart Sophia", "Meindl Noah", "Merkle Metilda",
                             "Milosavljevic Todor", "Morocutti Daniel", "Nitsche Leona", "Pertmayr-Floquet Marie",
                             "Pertmayr-Floquet Markus", "Rainer Nikki", "Rauscher Maja", "Rüdegger Emil",
                             "Strecha Sophie", "Trinkl Valentin", "Willminger Lino", "Zavatska Sofiia"]

        self.large_frame = ctk.CTkScrollableFrame(self, height=self.winfo_screenheight(),
                                                  width=math.floor(self.winfo_width()),
                                                  fg_color="black")
        self.large_frame.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")

        y = 0
        m = math.floor(self.large_frame.cget("width") // 256)
        if m >= 7:
            m = 6

        for x, student in enumerate(self.student_list):
            self.student_names = Frame(self.large_frame, value=student)
            if x % m == 0 and x != 0:
                y += 1
            self.student_names.grid(row=y, column=x % m, padx=15, pady=10)

        self.large_frame = None
        self.refreshButton = ctk.CTkButton(self, text="refresh", command=self.refresh)
        self.refreshButton.grid(row=0, column=1)
        self.refresh()

    def refresh(self):
        if self.large_frame is not None:
            self.large_frame.destroy()
        self.large_frame = ctk.CTkScrollableFrame(self, height=self.winfo_screenheight(),
                                                  width=math.floor(self.winfo_width()),
                                                  fg_color="black")
        self.large_frame.grid(row=0, column=0, padx=10, pady=10, sticky="nsew")

        y = 0
        m = math.floor(self.large_frame.cget("width") // 256)
        if m >= 7:
            m = 6

        for x, student in enumerate(self.student_list):
            self.student_names = Frame(self.large_frame, value=student)
            if x % m == 0 and x != 0:
                y += 1
            self.student_names.grid(row=y, column=x % m, padx=15, pady=10)

    def update_width(self):
        width = self.winfo_width() * 1.25
        return width


app = App()
app.mainloop()
