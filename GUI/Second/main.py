import customtkinter as ctk

name = "Benjamin"
username = "Haderak"
password = "qwertz"

ctk.set_appearance_mode("dark")
ctk.set_default_color_theme("dark-blue")

window = ctk.CTk()
window.geometry("500x400")

frame = ctk.CTkFrame(master=window)
frame.pack(padx=12, pady=10)

header = ctk.CTkLabel(master=frame, text="Login")
header.pack(padx=12, pady=10)

entry1 = ctk.CTkEntry(master=frame, placeholder_text="Username")
entry1.pack(padx=12, pady=10)

entry2 = ctk.CTkEntry(master=frame, placeholder_text="Password", show="*")
entry2.pack(padx=12, pady=10)


def check_login():
    u_name = entry1.get()
    pw = entry2.get()
    if u_name == username and pw == password:
        button_press_greeting()
        new_frame()

    else:
        error_text = "Wrong username or password. Try again!"
        wrong_login = ctk.CTkLabel(master=frame, text=error_text)
        wrong_login.pack(padx=12, pady=10)


def button_press_greeting():
    text = f"Welcome back, {name}!"
    greeting = ctk.CTkLabel(master=frame, text=text)
    greeting.pack(padx=12, pady=10)


def new_frame():
    frame.destroy()
    frame2 = ctk.CTkFrame(master=window)
    frame2.pack(padx=12, pady=10)


button = ctk.CTkButton(master=frame, text="Login", command=check_login)
button.pack(padx=12, pady=10)

optionmenu = ctk.CTkOptionMenu(master=frame, values=["Open", "Close"])
optionmenu.set("Close")
optionmenu.pack()

window.mainloop()
