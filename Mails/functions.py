import win32com.client as win32
import variablen as var

text = []
e_types = ['ms', 'ps', 'pt']
mail_addresses = []
subject = []

path = var.path


def exam_type():
    type_input = input('Art der Prüfung (ms/ps/pt):\n\t')
    if type_input not in e_types:
        print('Nur s oder t sind erlaubt!')
        return exam_type()
    elif type_input == 'ms':
        subject.append('Mathematikschularbeit')
        return 'die Mathematikschularbeit'
    elif type_input == 'ps':
        subject.append('Physikschularbeit')
        return 'die Physikschularbeit'
    elif type_input == "pt":
        subject.append('Physiktest')
        return 'den Physiktest'


exam_type = exam_type()


def mail_text_generator(name, ex_type, gender, points, mark):
    """returns the standard text depending on gender"""
    if gender == 'm':
        return fr"""
                   Lieber {name}!<br><br>
                   Du hast auf {ex_type} {points} Punkte erreicht, was ein {mark} ist.<br><br>
                   LG<br>
                    <blue>______________________________________<br>
                    <b>Benjamin Lux, MEd</b><br>
                    BG/BRG Perchtoldsdorf</blue><br>
                    Bundesgymnasium und Bundesrealgymnasium<br>
                    Roseggergasse 2–4,<br>
                    2380 Perchtoldsdorf<br>
                    <a\
                    href="www.bgperchtoldsdorf.at">www.bgperchtoldsdorf.at</a>
                """
    elif gender == 'w':
        return fr"""
                   Liebe {name}!<br><br>
                   Du hast auf {ex_type} {points} Punkte erreicht, was ein {mark} ist.<br><br>
                   LG<br>
                    ______________________________________<br>
                    <b>Benjamin Lux, MEd</b><br>
                    BG/BRG Perchtoldsdorf<br>
                    Bundesgymnasium und Bundesrealgymnasium<br>
                    Roseggergasse 2–4,<br>
                    2380 Perchtoldsdorf<br>
                    <a\
                    href="www.bgperchtoldsdorf.at">www.bgperchtoldsdorf.at</a>
                """


def mark_converter(note):
    """Converts integer marks to named marks"""
    if note == 1:
        return 'Sehr gut'
    elif note == 2:
        return 'Gut'
    elif note == 3:
        return 'Befriedigend'
    elif note == 4:
        return 'Genügend'
    elif note == 5:
        return 'Nicht genügend'


def mail_address_generator(name, f_name):
    mail_address = f'{name}.{f_name}@bgperchtoldsdorf.at'
    return mail_address


def var_to_text(ex_type):
    """Inputs the Excel list data into the text and stores it into a list"""
    i = 0
    while i < var.length:
        name = var.name_list[i]
        f_name = var.f_name_list[i]
        gender = var.gender_list[i]
        points = var.points_list[i]
        mark = mark_converter(var.mark_list[i])
        ex_type = ex_type
        mail_addresses.append(mail_address_generator(name, f_name))
        text.append(mail_text_generator(name, ex_type, gender, points, mark))

        i += 1


def mail_sender(ex_type):
    i = 0

    while i < var.length:
        var_to_text(ex_type)

        outlook = win32.Dispatch('outlook.application')
        mail = outlook.CreateItem(0)

        mail.Subject = f'Note {subject[0]}'
        mail.To = mail_addresses[i]

        mail.HTMLBody = text[i]
        mail.Send()

        i += 1
