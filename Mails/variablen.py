import pandas as pd

path = input('Dateipfad:\n\t')

# reads by default 1st sheet of an Excel file
required_data = pd.read_excel(path)

f_name_list = required_data['Familienname'].tolist()
name_list = required_data['Vorname'].tolist()
points_list = required_data['Punkte'].tolist()
mark_list = required_data['Note'].tolist()
gender_list = required_data['Geschlecht'].tolist()

length = len(f_name_list)
