import pandas as pd

filename = 'NABE.xlsx'
sup = []
sups = {}

data = pd.read_excel(filename)


def get_short(col):
    for name in col:
        if name != 'x':
            sup.append(name)
        else:
            continue


def count(supplierungen):
    for supplierung in supplierungen:
        supplierung = supplierung.replace(' ', '')
        if supplierung in sups.keys():
            sups[supplierung] += 1
        else:
            sups[supplierung] = 1


def write_list(dict):
    nabe = open('NaBe-Kalender.txt', 'w')
    for k, v in dict.items():
        key_value = f'{k}:\t{v}\n'
        nabe.write(key_value)


def main():
    for i in range(6):
        get_short(data[i])
    print(sup)
    count(sup)
    print(sups)
    write_list(sups)


if __name__ == '__main__':
    main()
