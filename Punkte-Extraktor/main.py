import pandas as pd

path = input('Dateipfad:\n\t')
data = pd.read_excel(path)

points = data['Punkte'].tolist()


def write_points_to_file(pts):
    punkte = open('Punkte.txt', 'w')
    for point in pts:
        punkte.write(f'{point}\n')


def main():
    write_points_to_file(points)


if __name__ == '__main__':
    main()
