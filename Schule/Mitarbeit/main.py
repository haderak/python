path = "C:\Python\Schule\Mitarbeit\Klassenliste.txt"


class Student:
    def __init__(self, first_name, last_name, class_name, subject):
        self.first_name = first_name
        self.last_name = last_name
        self.class_name = class_name
        self.subject = subject

        self.class_dict = {}



if __name__ == '__main__':
    student = Student("Benjamin", "Lux", "8B", "Physik")
    student.create_class_list()
