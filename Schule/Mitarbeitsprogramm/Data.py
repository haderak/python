import json
import pandas as pd
import os

import Variables
import Widgets


class Data:
    def __init__(self):
        self.name_list = []
        self.class_name = input('Klassenname:\t')
        self.class_subject = input('Fach:\t')

        self.folder_path = f'C:\Python\Schule\Mitarbeitsprogramm\Klassen\{self.class_name}\{self.class_subject}'
        if not os.path.exists(self.folder_path):
            os.makedirs(self.folder_path)

        self.class_names_file = f'{self.class_name}_{self.class_subject}.json'
        self.file_path = os.path.join(self.folder_path, self.class_names_file)

        self.create_name_list()

        self.student_mitarbeit_dict = self.create_mitarbeit_dict()

    def create_name_list(self):
        """Create the names of the individual students from an Excel list"""
        student_names = pd.read_excel(
            rf'{Variables.student_list}')
        family_names = student_names["Familienname"].tolist()
        first_names = student_names["Vorname"].tolist()

        for family_name, first_name in zip(family_names, first_names):
            self.name_list.append(f"{family_name} {first_name}")

        with open(self.file_path, 'w') as file:
            json.dump(self.name_list, file)

    def create_mitarbeit_dict(self):
        """Creates a dict with the name of the student and the Mitarbeit + date"""
        with open(self.file_path) as file:
            return json.load(file)

    def create_date_mitarbeit_dict(self, date, mitarbeit):
        """Creates a dictionary with the date as key and the mitarbeit as value"""

    def add_date_and_mitarbeit(self, student):
        """Adds a dictionary as a value to the name dictionary with the date as key and the mitarbeit as value"""
        for name in self.student_mitarbeit_dict:
            name[current_datetime] = student.mitarbeit_label.cget('text')
