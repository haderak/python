import customtkinter as ctk

import ScrollFrame
import Variables
import Data
from datetime import datetime

current_datetime = datetime.now().strftime("%d.%m.")


class MainWindow(ctk.CTk):
    def __init__(self):
        super().__init__()
        self.title = 'Mitarbeit 1.2'
        self.geometry('1200x400')
        self.iconbitmap(Variables.icon_file_path)

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)

        # Scrollable Frame
        self.scroll_frame = ScrollFrame.ScrollFrame(self, fg_color="black")
        self.scroll_frame.grid(row=1,
                               column=0,
                               padx=10,
                               pady=50,
                               sticky='nsew'
                               )

        self.save_combobox = ctk.CTkComboBox(self,
                                             values=['Neue Klasse', 'Speichern']
                                             )
        self.save_combobox.place(relx=.01,
                                 rely=.025
                                 )
