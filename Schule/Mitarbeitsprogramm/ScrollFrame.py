import customtkinter as ctk

import Widgets
import Data


class ScrollFrame(ctk.CTkScrollableFrame):
    """Creates a scrollable frame as a master for the individual student frames"""

    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)

        self.grid_columnconfigure((0, 1, 2, 3, 4, 5), weight=1)
        self.grid_rowconfigure((0, 1, 2, 3, 4, 5, 6), weight=1)

        # Data
        self.data = Data.Data()

        self.name_list = self.data.name_list

        self.student_list = []

        # Create the students Frame
        y = 0

        for x, student in enumerate(self.name_list):
            # creates the students frame
            self.widget_frame = Widgets.WidgetFrame(self,
                                                    student_name=student
                                                    )
            if x % 4 == 0 and x != 0:
                y += 1

            self.widget_frame.grid(row=y, column=x % 4, pady=10)
            self.student_list.append(self.widget_frame)
