from datetime import datetime

file_datetime = datetime.now().strftime("%Y%m%d")
mitarbeit_datetime = datetime.now().strftime("%d.%m.%y")

student_label_fg_color = "#1194ee"  # blue
student_label_absence_color = "#f30c05"  # red
student_label_called_color = "#F96A06"  # orange

minus_button_fg_color = "#F68EFF"  # pink
minus_button_hover_color = "#ac00bb"  # darker pink

plus_button_fg_color = "#48CE3D"  # green
plus_button_hover_color = "#258c1c"  # darker green

grayed_out_color = "#808080"  # gray

icon_file_path = 'C:\Python\Schule\Mitarbeitsprogramm\education.ico'

student_list = r"C:\Users\BenjaminLux\OneDrive - BG Perchtoldsdorf\!Schule\!Physik\7. Klasse\Klassenliste 7Cb.xlsx"

