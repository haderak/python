import customtkinter as ctk
import Variables


class WidgetFrame(ctk.CTkFrame):
    def __init__(self, master, student_name, **kwargs):
        super().__init__(master, **kwargs)

        self.grid_columnconfigure((0, 2), weight=1)
        self.grid_rowconfigure((0, 1, 2), weight=1)
        self.grid_columnconfigure(1, weight=2)

        self.student_name = student_name
        self.mitarbeit = ctk.IntVar(value=0)
        self.absence = ctk.StringVar(value='f')

        self.student_label = ctk.CTkLabel(self,
                                          text=self.student_name,
                                          text_color='black',
                                          fg_color=Variables.student_label_fg_color,
                                          corner_radius=6,
                                          font=('Arial', 14, 'bold')
                                          )
        self.student_label.grid(row=0,
                                column=0,
                                columnspan=3
                                )

        # Attendance switch
        self.attendance_switch = ctk.CTkSwitch(self,
                                               text='Anwesend',
                                               fg_color='green',
                                               progress_color='red',
                                               command=self.absent_student
                                               )
        self.attendance_switch.grid(row=1,
                                    column=0,
                                    padx=5,
                                    sticky='w',
                                    pady=5
                                    )

        # Called checkbox
        self.called_checkbox = ctk.CTkCheckBox(self,
                                               text='Dran\ngenommen',
                                               )
        self.called_checkbox.grid(row=1,
                                  column=2,
                                  padx=5,
                                  sticky='w',
                                  pady=5
                                  )

        # Minus button
        self.minus_button = ctk.CTkButton(self,
                                          text="-",
                                          width=90,
                                          text_color="black",
                                          fg_color=Variables.minus_button_fg_color,
                                          hover_color=Variables.minus_button_hover_color,
                                          command=self.minus,
                                          corner_radius=6
                                          )
        self.minus_button.grid(row=2,
                               column=0,
                               sticky="w",
                               pady=5
                               )

        # Plus Button
        self.plus_button = ctk.CTkButton(self,
                                         text="+",
                                         width=90,
                                         text_color="black",
                                         fg_color=Variables.plus_button_fg_color,
                                         hover_color=Variables.plus_button_hover_color,
                                         command=self.plus,
                                         corner_radius=6
                                         )
        self.plus_button.grid(row=2,
                              column=2,
                              sticky="e",
                              pady=5,
                              )

        # Mitarbeit Label
        self.mitarbeit_label = ctk.CTkLabel(self,
                                            textvariable=self.mitarbeit,
                                            width=46,
                                            fg_color="gray30",
                                            corner_radius=6
                                            )
        self.mitarbeit_label.grid(row=2,
                                  column=1,
                                  sticky='we',
                                  )

    def plus(self):
        """Adds 1 to the Mitarbeit"""
        self.mitarbeit.set(self.mitarbeit.get() + 1)

    def minus(self):
        """Adds 1 to the Mitarbeit"""
        self.mitarbeit.set(self.mitarbeit.get() - 1)

    def absent_student(self):
        """Changes the color of the name tag and the buttons
        + deactivates the buttons, when the attendance switch is turned on.
        If the switch is turned off, it resets"""

        if self.attendance_switch.get() == 1:
            self.student_label.configure(True,
                                         fg_color=Variables.student_label_absence_color  # red
                                         )

            self.minus_button.configure(True,
                                        fg_color=Variables.grayed_out_color,  # gray
                                        hover_color=Variables.grayed_out_color,
                                        command=None
                                        )

            self.plus_button.configure(True,
                                       fg_color=Variables.grayed_out_color,  # gray
                                       hover_color=Variables.grayed_out_color,
                                       command=None
                                       )

            self.mitarbeit_label.configure(True,
                                           textvariable=self.absence
                                           )

            self.called_checkbox.configure(True,
                                           state='disabled'
                                           )
        else:
            self.student_label.configure(True,
                                         text=self.student_name,
                                         text_color='black',
                                         fg_color=Variables.student_label_fg_color,
                                         corner_radius=6,
                                         font=('Verdana', 14, 'bold')
                                         )

            # Attendance switch
            self.attendance_switch.configure(True,
                                             text='Anwesend',
                                             fg_color='green',
                                             progress_color='red',
                                             command=self.absent_student
                                             )

            # Called checkbox
            self.called_checkbox.configure(True,
                                           state='normal'
                                           )

            # Minus button
            self.minus_button.configure(True,
                                        text="-",
                                        width=90,
                                        text_color="black",
                                        fg_color=Variables.minus_button_fg_color,
                                        hover_color=Variables.minus_button_hover_color,
                                        command=self.minus,
                                        corner_radius=6
                                        )

            # Plus Button
            self.plus_button.configure(True,
                                       text="+",
                                       width=90,
                                       text_color="black",
                                       fg_color=Variables.plus_button_fg_color,
                                       hover_color=Variables.plus_button_hover_color,
                                       command=self.plus,
                                       corner_radius=6
                                       )
            # Mitarbeit Label
            self.mitarbeit_label.configure(True,
                                           textvariable=self.mitarbeit,
                                           width=46,
                                           fg_color="gray30",
                                           corner_radius=6
                                           )
