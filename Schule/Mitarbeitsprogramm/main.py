import customtkinter as ctk

import Data
import MainWindow

ctk.set_appearance_mode("dark")
ctk.set_default_color_theme("dark-blue")
ctk.deactivate_automatic_dpi_awareness()

if __name__ == '__main__':
    app = MainWindow.MainWindow()
    app.mainloop()
