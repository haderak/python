import random


def throw_die(faces):
    value = random.randint(1, faces)
    return value


def create_dict(dice_number, throwing_number, faces):
    throw_list = []
    throw_dict = {}

    die = 1
    number = 1

    while number <= throwing_number:
        while die <= dice_number:
            throw = int(throw_die(faces))
            throw_list.append(throw)
            die += 1

        sixes_count = throw_list.count(6)
        dice_number = dice_number - sixes_count

        throw_dict[number] = dice_number

        number += 1

        die = 1
        throw_list = []

    return throw_dict


def create_txt_files():
    dice_number = input("Wie viele Würfel sollen geworfen werden?\n\t")
    throwing_number = input("Wie oft soll gewürfelt werden?\n\t")
    faces = input("Wie viele Seiten hat ein Würfel?\n\t")
    reps = input("Wie oft soll der Versuch wiederholt werden?\n\t")

    dices = open("Würfel.txt", "w")

    dices.write('\t')

    for throw in range(1, int(throwing_number) + 1):
        dices.write(f'{throw}. Wurf\t')
    dices.write('\n')

    for rep in range(1, int(reps) + 1):
        throw_dict = create_dict(int(dice_number), int(throwing_number), int(faces))

        dices.write(f'{rep}. Versuch\t')

        for num, die in throw_dict.items():
            dices.write(f'{die} \t')

        dices.write('\n')


if __name__ == '__main__':
    create_txt_files()
