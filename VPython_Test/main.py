from vpython import *

scene.background = color.white

xaxis_1 = cylinder(pos=vector(0, 0, 0), axis=vector(5, 0, 0), radius=0.01, color=color.gray(0.5))
xaxis_2 = cylinder(pos=xaxis_1.pos, axis=vector(-5, 0, 0), radius=xaxis_1.radius, color=xaxis_1.color)
yaxis_1 = cylinder(pos=xaxis_1.pos, axis=vector(0, 5, 0), radius=xaxis_1.radius, color=xaxis_1.color)
yaxis_2 = cylinder(pos=xaxis_1.pos, axis=vector(0, -5, 0), radius=xaxis_1.radius, color=xaxis_1.color)
zaxis1 = cylinder(pos=xaxis_1.pos, axis=vector(0, 0, 5), radius=xaxis_1.radius, color=xaxis_1.color)
zaxis_2 = cylinder(pos=xaxis_1.pos, axis=vector(0, 0, -5), radius=xaxis_1.radius, color=xaxis_1.color)

bar = cylinder(pos=vector(0, 0, 0), axis=vector(2, 0, 0), radius=.25, color=color.red)
weight_1 = sphere(pos=bar.pos, radius=bar.radius * 3, color=bar.color)
weight_2 = sphere(pos=bar.pos + bar.axis, radius=bar.radius * 3, color=bar.color)


