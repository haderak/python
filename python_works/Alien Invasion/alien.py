import pygame
from pygame.sprite import Sprite


class Alien(Sprite):
    """A class to manage the aliens"""

    def __init__(self, ai_game):
        """Initialize the alien and set its starting position"""
        super().__init__()

        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.screen_rect = ai_game.screen.get_rect()

        # Load the alien and get its rect
        self.image = pygame.image.load(self.settings.alien_list[self.settings.alien_colour])
        self.rect = self.image.get_rect()

        # Start the first alien on the top left corner of the screen
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        # Convert rect to a decimal point
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

    def check_edges(self):
        """Return True if alien is at the edge of the screen"""

        screen_rect = self.screen.get_rect()
        if self.rect.right >= screen_rect.right or self.rect.left <= 0:
            return True

    def update(self):
        """Move the aliens right or left"""

        self.x += (self.settings.alien_speed *
                   self.settings.fleet_direction)
        self.rect.x = self.x
