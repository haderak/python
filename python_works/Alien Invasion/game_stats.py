class GameStats:
    """Track statistics for Alien Invasion"""

    def __init__(self, ai_game):
        """Initialize statistics"""
        self.settings = ai_game.settings
        self.reset_stats()

        # Starts Alien Invasion in an inactive state
        self.game_active = False

        # Sets a counter for destroyed fleets
        self.fleets_destroyed = 0

    def reset_stats(self):
        """Initialize statistics that can change during the game"""

        self.ships_left = self.settings.ship_limit
