class Settings:
    """A class to store all settings for Alien Invasion"""

    def __init__(self):
        """Initialize the game's settings"""

        # Screen settings
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_colour = (153, 153, 255)

        # Ship settings
        self.ship_speed = 1.5
        self.ship_limit = 3

        # Bullet settings
        self.bullet_speed = 1.0
        self.bullet_width = 5000
        self.bullet_height = 15
        self.bullet_colour = (255, 0, 0)
        self.bullets_allowed = 5

        # Alien settings
        self.alien_speed = 0.5
        self.fleet_drop_speed = 10

        # Speeding up of the aliens
        self.alien_speed_increase = 0.2
        self.fleet_drop_speed_increase = 0.5

        # Fleet direction of 1 represents right; -1 represents left
        self.fleet_direction = 1

        self.alien_list = ['images/cute_alien.bmp', 'images/classic_alien.bmp', 'images/weird_alien.bmp',
                           'images/green_alien.bmp', 'images/red_alien.bmp', 'images/blue_alien.bmp']
        self.alien_colour = 0
