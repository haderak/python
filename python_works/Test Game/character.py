import pygame


class Character:
    """A class to manage the character"""

    def __init__(self, tg_game):
        """Initialize the character and set his starting position"""
        self.screen = tg_game.screen
        self.settings = tg_game.settings
        self.screen_rect = tg_game.screen.get_rect()

        # Load the char and its rect
        self.image = pygame.image.load('images/char.bmp')
        self.rect = self.image.get_rect()

        # Start the char at the bottom left of the screen
        self.rect.bottomleft = self.screen_rect.bottomleft

        # Store a decimal value for the char's movements
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

        # Movement Flag
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False

    def update_char(self):
        """Update the character's position and movement"""

        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.char_speed
        if self.moving_left and self.rect.left > 0:
            self.x -= self.settings.char_speed
        if self.moving_up and self.rect.top > 0:
            self.y -= self.settings.char_speed
        if self.moving_down and self.rect.bottom < self.screen_rect.bottom:
            self.y += self.settings.char_speed

        # Update rect object
        self.rect.x = self.x
        self.rect.y = self.y

    def blitme(self):
        """Draw the character at it's current location"""
        self.screen.blit(self.image, self.rect)
