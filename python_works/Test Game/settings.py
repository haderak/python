class Settings:
    """A class for all the settings in Test Game"""

    def __init__(self):
        """Initialize the game's settings"""

        # Screen settings
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_colour = (125, 25, 25)

        # Speed setting
        self.char_speed = .5