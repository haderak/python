import sys

import pygame

from settings import Settings
from character import Character


class TestGame:
    """Overall class to manage the game"""

    def __init__(self):
        """Initialize the game, and create resources"""

        pygame.init()

        self.settings = Settings()

        self.screen = pygame.display.set_mode(
            (self.settings.screen_width, self.settings.screen_height)
        )
        pygame.display.set_caption("Test Game")

        self.character = Character(self)

    def run_game(self):
        """Start the main loop for the game"""
        while True:
            self._update_screen()
            self.character.update_char()
            self._check_events()

    def _check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self._check_event_keypress(event)
            elif event.type == pygame.KEYUP:
                self._check_event_keyrelease(event)

    def _update_screen(self):
        self.screen.fill(self.settings.bg_colour)
        self.character.blitme()

        pygame.display.flip()

    def _check_event_keypress(self, event):
        """Respond to key presses"""

        if event.key == pygame.K_RIGHT:
            self.character.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.character.moving_left = True
        elif event.key == pygame.K_UP:
            self.character.moving_up = True
        elif event.key == pygame.K_DOWN:
            self.character.moving_down = True
        elif event.key == pygame.K_e:
            sys.exit()

    def _check_event_keyrelease(self, event):
        """Respond to key release"""

        if event.key == pygame.K_RIGHT:
            self.character.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.character.moving_left = False
        elif event.key == pygame.K_UP:
            self.character.moving_up = False
        elif event.key == pygame.K_DOWN:
            self.character.moving_down = False


if __name__ == '__main__':
    tg = TestGame()
    tg.run_game()
